export default {
    primary: '#e63946',
    secondary: '#0984e3',
    black: '#000',
    white: '#fff',
    light: "#f0f8ff",
    danger: '#ff5252'
    };