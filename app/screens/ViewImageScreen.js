import React from 'react';
import { View, StyleSheet, Image } from 'react-native';

import colors from '../config/colors'; 

function ViewImageScreen(props) {
    return(
        <View style={styles.container}>
            <View style={styles.closeButton}></View>
            <View style={styles.deleteButton}></View>
            <Image style={styles.image} source={require("../assets/image-view.jpeg")} resizeMode="contain"/>
        </View>
    );
}

const styles = StyleSheet.create({
    image: {
        width: "100%",
        height: "100%",
    },
    container: {
        backgroundColor: colors.black,
    },
    closeButton: {
        width: 50,
        height: 50,
        position: "absolute",
        top: 25,
        left: 30,
        backgroundColor: colors.primary,
    },
    deleteButton: {
        width: 50,
        height: 50,
        position: "absolute",
        top: 25,
        right: 30,
        backgroundColor: colors.secondary,
    },
})
export default ViewImageScreen;