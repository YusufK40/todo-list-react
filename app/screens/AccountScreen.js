import React from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import AppText from '../components/AppText';
import Listitem from '../components/Listitem';

function AccountScreen(props) {
    return (
        <View>
            <View style={styles.accountContainer}>
                <AppText style={styles.account}>Account</AppText>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    accountContainer: {
        width: '100%',
        height: '100%',
    },

    account: {
        textAlign: 'center',
        marginTop: 50,
        fontSize: 35,
    },

    item1: {

    }
})

export default AccountScreen;