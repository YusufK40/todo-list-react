import React from "react";
import { View, Text, Image, StyleSheet, ImageBackground } from "react-native";
import AppButton from "../components/AppButton";

function WelcomeScreen(props) {
  return (
    <ImageBackground
    blurRadius={5}
      style={styles.background}
      source={require("../assets/background.jpeg")}
    >
      <View style={styles.logoContainer}>
        <Image style={styles.logo} source={require("../assets/logo-red.png")} />
        <Text style={styles.tagline}>Sell what you don't need</Text>
      </View>

      <View style={styles.buttonContainer}>      
        <AppButton title="login" />
        <AppButton title="register" color="secondary" />
      </View>

    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },

  loginButton: {
    width: "100%",
    height: 70,
    backgroundColor: "#fc5c65",
  },

  logo: {
    width: 100,
    height: 100,
  },

  logoContainer: {
    position: "absolute",
    top: 70,
    alignItems: "center",
  },

  buttonContainer: {
    width: "100%",
    padding: 20,
  },

  tagline: {
    fontSize: 25,
    fontWeight: "600",
    marginVertical: 20,

  },
});
export default WelcomeScreen;
