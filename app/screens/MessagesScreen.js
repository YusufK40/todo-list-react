import React, { useState } from "react";
import { FlatList, StyleSheet, View } from "react-native";

import Listitem from "../components/Listitem";
import ListItemDeleteAction from "../components/ListItemDeleteAction";
import ListitemSeperator from "../components/ListitemSeperator";
import Screen from "../components/Screen";

const initialMessages = [
  {
    id: 1,
    title: "Bericht 1",
    description: "Hier komt het berciht",
    image: require("../assets/peron.jpg"),
  },

  {
    id: 2,
    title: "Bericht 2",
    description: "Hier komt het bericht",
    image: require("../assets/peron.jpg"),
  },
];

function MessagesScreen(props) {

  const [messages, setMessages] = useState(initialMessages);
  const [refreshing, setRefreshing] = useState(false);

  const handleDelete = (message) => {
    setMessages(messages.filter((m) => m.id !==message.id));
  }

  return (
    <Screen>
      <FlatList
        style={styles.container}
        ItemSeparatorComponent={ListitemSeperator}
        data={messages}
        refreshing={refreshing}
        onRefresh={() => setMessages(initialMessages)}
        keyExtractor={(message) => message.id.toString()}
        renderItem={({ item }) => (
          <Listitem
            title={item.title}
            subTitle={item.description}
            image={item.image}
            onPress={() => console.log("dit is een bericht:", item)}
            renderRightActions={() => (
              <ListItemDeleteAction
                onPress={() => {
                  handleDelete(item);
                }}
              />
            )}
          />
        )}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%"
  }
});

export default MessagesScreen;
