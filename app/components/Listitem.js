import React from 'react';
import { View, StyleSheet, Image, TouchableHighlight } from 'react-native';
import colors from '../config/colors';
import AppText from './AppText';
import { Swipeable } from 'react-native-gesture-handler';

function Listitem({title, subTitle, image, onPress, renderRightActions, ImageComponent}) {
    return (
    <Swipeable renderRightActions={renderRightActions}>
        <TouchableHighlight onPress={onPress} underlayColor={colors.light}>    
            <View style={styles.listitem}>
                {ImageComponent}
                {image && <Image style={styles.image} source={image}/>}

                <View style={styles.text}>
                    <AppText style={styles.name}>{title}</AppText>
                    <AppText style={styles.amount}>{subTitle}</AppText>
                </View>
            </View>
        </TouchableHighlight>
    </Swipeable>
    );
}

const styles = StyleSheet.create({
    listitem: {
        marginTop: 10,
        flexDirection: 'row',
        padding: 15,
    },
    
    image: {
        width: 50,
        height: 50,
        borderRadius: 50,
    },

    text: {
        paddingLeft: 10,
    },

    amount: {
        fontSize: 14,
        color: '#cacfd2'
    }
})

export default Listitem;